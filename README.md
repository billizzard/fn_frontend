## Start prod environment

#### Create prod image

```bash
docker build -t "app/front:latest" -f docker/prod/Dockerfile .
```

This image is not used by itself, it needs for backend prod image.

## Start local environment

```bash
docker-compose -f docker-compose.local.yml up
```
#### Install dependencies locally for ide

```bash
yarn install
```

### Front check url
```bash
http://localhost:3000
```
